# Tratado de datos en forma de pila

El presente programa solicita el ingreso de datos que son guardados en un formato pila. Se pueden agregar cierta cantidad de datos, cantidad previamente solicitada. No se pueden agregar más y al momento de eliminar el primero que sale es el último que entra. 

[Revisar estructura de una pila aquí](https://2.bp.blogspot.com/-6WFr7JP1Ke0/VsiVAgxSJBI/AAAAAAAAAFY/TOJTsayaKhI/s1600/Pilas%2Ben%2Bc%252B%252B.jpg)

## Para comenzar

En primera instancia se solicita la capacidad de la pila. Luego se muestra un menú donde se puede agregar datos a la pila, eliminar datos de la pila, ver la pila o salir del programa. 

No se podrán agregar datos a la pila una vez que esté llena ni eliminar una vez esté vacía. Estos datos se van agregando en el estilo de la pila, el último que se agrega está en la parte superior y, en caso de eliminar, se debe ir primero el último ingresado; no se puede acceder a datos debajo del último/superior dato.

## Prerrequisitos

### Sistema Operativo

Para ejecutar el programa se necesita un sistema operativo Linux, de preferencia Ubuntu o Debian.

### Make

Se necesita instalar un make para compilar el programa de manera sencilla. Se instala mediante el siguiente comando:

`sudo apt install make`

**En caso de no usar make**

Se puede ejecutar el programa sin make (revisar **Ejecutando las pruebas**).

## Ejecutando las pruebas 

### Iniciar programa "compilar"

**Con make**

En la terminal situada en la carpeta con nuestros archivos se debe ejecutar el comando:

`make`

**Sin make**

En la terminar situada en la carpeta con nuestros archivos se debe ejecutar el siguiente comando:

`g++ Programa.cpp Dato.cpp -o programa`

### Luego de compilar

Para ejecutar el programa una vez compilado se debe ejecutar el siguiente comando:

`./programa`

El programa solicitará el ingreso de la capacidad máxima que tendrá la pila. Esta puede ser mayor a 0, no menor o igual ya que no existe esa capacidad; no existiría la pila. En caso de ingresar un dato no válido como números negativos o caracteres el programa se detendrá.

Posterior al ingreso de la capacidad de la pila se nos abrirá un menú con las siguientes opciones:

**1. Agregar elemento a la pila:** Esta opción nos solicitará el elemento a ingresar a nuestra pila, el cual puede ser cualquier tipo de dato. Este se agregará siempre y cuando la pila tenga espacio para agregar más elementos, es decir cuando la pila no esté llena. Si está llena retornará un error de overflow y volverá al menú.

**2. Eliminar elemento de la pila:** Esta opción eliminará automáticamente el último elemento de la pila debido a que así es como funciona este tratamiento de datos. En caso de que la pila esté vacía retornará un error underflow y volverá al menú.

**3. Ver pila:** Esta opción mostrará la pila y sus datos apilado en la forma que debe tener. El primero en ingresar está por debajo mientras que el último está de los primeros. 

**4. Salir:** Tal y como lo explica el nombre, este es para salir del programa. 

Las 3 primeras opciones retornan al menú en caso de seguir interactuando con la pila, mientras que la opción 4 y cualquier otra detienen el programa.

## Despliegue

La aplicación se debe ejecutar desde la terminal localizada en la carpeta que presenta los archivos de ésta. Se debe de ejecutar bajo el sistema operativo Linux (señalado en Prerrequisitos).

## Construido con
- Lenguaje c++: librería iostream.

## Autor
- Noemí Gómez Rodríguez - nogomez19@alumnos.utalca.cl 
