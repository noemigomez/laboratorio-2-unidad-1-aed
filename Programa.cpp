#include <iostream>
#include "Dato.h"
using namespace std;

void imprimir_pila(int max, Dato pila[], int tope)
{
  //para ver lo que llevamos de pila se usa el tope!
  cout << "-----------------------------------" << endl;
  for(int i=tope; i>-1; i--){
    cout << "[" << pila[i].get_dato() << "]" << endl;
  }
  cout << "-----------------------------------" << endl;
}

void pila_llena(int tope, int max, bool &band)
{
  /*
      tope+1 porque si el max es 2 los indices de la pila son 0 y 1
      por lo tanto el tope que llega cuando la pila está llena es 1
      y el max es 2, por eso tope+1==max
  */
  if(tope+1==max){
    band = true;
  }
  else{
    band = false;
  }
}

void push(int max, Dato pila[], int &tope, string dato)
{
  Dato data = Dato();
  bool band;
  pila_llena(tope, max, band);
  // si la pila está llena, band is true
  if(band){
    cout << "Overflow, dato no agregado porque la pila está llena" << endl;
  }
  // pila no llena, agrega y aumenta el tope
  else{
    data.set_dato(dato);
    tope = tope + 1;
    pila[tope] = dato;
  }
}

void pila_vacia(int tope, bool &band)
{
  // tope -1 ya que el índice menor de la pila es 0, para que esté vacía tiene que ser -1
  if(tope==-1){
    band = true;
  }
  else{
    band = false;
  }
}

void pop(Dato pila[], int &tope)
{
  bool band;
  pila_vacia(tope, band);

  if(band){
    cout << "Underflow, pila vacía, nada que eliminar." << endl;
  }
  else{
    cout << "Elimado último elemento: "  << pila[tope].get_dato() << endl;
    tope = tope - 1;
  }
}

void menu(int max, Dato pila[], int tope)
{
  int opcion;
  cout << "Ingrese opción: " << endl;
  cout << "|1| Agregar elemento a pila." << endl;
  cout << "|2| Eliminar objeto de la pila." << endl;
  cout << "|3| Ver pila." << endl;
  cout << "|4| Salir." << endl;
  cin >> opcion;

  if(opcion==1){
    string dato;
    cout << "Dato que desee agregar: ";
    cin >> dato;
    push(max, pila, tope, dato);
    menu(max, pila, tope);
  }
  else if(opcion==2){
    pop(pila, tope);
    menu(max, pila, tope);
  }
  else if(opcion==3){
    imprimir_pila(max, pila, tope);
    menu(max, pila, tope);
  }
  else if(opcion==4){
    cout << "Adiós." << endl;
  }
  else{
    cout << "Opción no válida." << endl;
  }
}


int main(int argc, char **argv)
{
  // máximo de una pila
  int max;
  cout << "Ingrese capacidad máxima de la pila: ";
  cin >> max;

  if(max>0){
    Dato pila[max];
    // comienza con -1 ya que el primer dato debe ser indice 0
    int tope = -1;
    menu(max, pila, tope);
  }
  else{
    cout << "Capacidad inválida" << endl;
  }
  return 0;
}
