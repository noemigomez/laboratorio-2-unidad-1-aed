#include <iostream>
using namespace std;

#ifndef DATO_H
#define DATO_H

class Dato {
    private:
        string dato = "\0";

    public:
        Dato();
        Dato (string dato);

        // metodos set y get
        void set_dato(string dato);
        string get_dato();


};
#endif
